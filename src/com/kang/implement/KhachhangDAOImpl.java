package com.kang.implement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.kang.dao.KhachhangDAO;
import com.kang.model.Khachhang;

public class KhachhangDAOImpl implements KhachhangDAO {

//	public static Connection con;
	@Override
	public void addKhachhang(Khachhang kh) {
		// TODO Auto-generated method stub
		Connection con;
		String sql = "insert into khachhang value(?,?,?,?,?,?)";
		try {
			con = DBConnect.getConnecttion();
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(sql);
			ps.setInt(1, kh.getId());
			ps.setString(2, kh.getHoten());
			ps.setString(3, kh.getSdt());
			ps.setString(4, kh.getGhichu());
			ps.setString(5, kh.getEmail());
			ps.setString(6, kh.getDiachi());
			ps.executeUpdate();
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public List<Khachhang> getList() {
		List<Khachhang> list = new ArrayList<Khachhang>();
		try {
			Connection con = DBConnect.getConnecttion();
			String sql = "select * from khachhang";
			if (con != null) {

				PreparedStatement ps = (PreparedStatement) con.prepareStatement(sql);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
//					Oto oto = (Oto) rs.getObject("oto");
					int id = rs.getInt("id");
					String hoten = rs.getString("hoten");
					String sdt = rs.getString("sodt");
					String ghichu = rs.getString("ghichu");
					String email = rs.getString("email");
					String diachi = rs.getString("diachi");
					list.add(new Khachhang(id, hoten, sdt, ghichu, email, diachi));
				}
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public Khachhang getKhachhhang(int id) {
		Khachhang kh = new Khachhang();
		try {
			Connection con = DBConnect.getConnecttion();

			String sql = "select * from khachhang where id='" + id + "'";
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				id = rs.getInt("id");
				String hoten = rs.getString("hoten");
				String sdt = rs.getString("sodt");
				String ghichu = rs.getString("ghichu");
				String email = rs.getString("email");
				String diachi = rs.getString("diachi");
				kh = new Khachhang(id, hoten, sdt, ghichu, email, diachi);
			}
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return kh;
	}

	@Override
	public List<Khachhang> searchList(String hoten) {
		String sql = null;
		if (!hoten.equals("")) {
			sql = "select * from khachhang where hoten='" + hoten + "'";

		}
		List<Khachhang> list = new ArrayList<Khachhang>();
		try {
			Connection con = DBConnect.getConnecttion();
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
//				Oto oto = (Oto) rs.getObject("oto");
				int id = rs.getInt("id");
				hoten = rs.getString("hoten");
				String sdt = rs.getString("sodt");
				String ghichu = rs.getString("ghichu");
				String email = rs.getString("email");
				String diachi = rs.getString("diachi");
				list.add(new Khachhang( id, hoten, sdt, ghichu, email, diachi));
			}
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	public static void main(String[] args) {
		Khachhang kh = new Khachhang(2, "Nguyen Van B", "037725394", "vip", "nguyenB@gmail.com", "HaNoi");
		KhachhangDAOImpl khaImpl = new KhachhangDAOImpl();
//		khaImpl.addKhachhang(kh);
		System.out.println(khaImpl.searchList("KimGiang"));
	}

	@Override
	public Object getKhachhhangByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}
}
