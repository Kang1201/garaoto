
Modul "Nhận xe vào gara" (2.3) với mô tả chi tiết nghiệp vụ: KH đem xe đến → QL
nhận xe và kiểm tra kĩ thuật sau đó chọn menu nhận xe → trang tìm KH theo tên hiện
ra → QL nhập tên KH và tìm → giao diện danh sách các KH có tên chứa từ khóa vừa
nhập hiện ra (nếu chưa có thì thêm mới KH) → QL click đúng tên KH → giao diện
danh sách các xe của KH đó đã từng sửa hiện ra (nếu chưa có thì click thêm xe mới
cho KH) → giao diện nhập các dịch vụ và phụ tùng hiện ra, QL lặp các bước sau cho
đến khi hết các dịch vụ/linh kiện theo yêu cầu của KH: QL click thêm dịch vụ/linh
kiện → giao diện tìm kiếm dịch vụ/linh kiện hiện ra → QL nhập tên và tìm → giao
diện danh sách các dịch vụ/linh kiện có tên chứa từ khóa vừa nhập hiện ra → QL click
chọn dịch vụ/linh kiện + nhập số lượng (giá đã có sẵn ở thời điểm hện tại) và xác nhận
→ dịch vụ/linh kiện được thêm vào hóa đơn tạm cho khách → Sau khi thêm xong các
dịch vụ/linh kiện theo yêu cầu KH, QL click xác nhận → giao diện chọn NV kĩ thuật
và slot hiện ra chứa các NV và slot đang rảnh (nếu không có thì chọn NV đang bận,
slot đang bận nhưng phải chờ) và xác nhận → hệ thống lưu lại và in ra hóa đơn tạm để
gắn vào xe đưa cho NV thực hiện.