package com.kang.dao;

import java.util.List;

import com.kang.model.Thanhvien;

public interface ThanhvienDAO {

	public boolean checkUser(String username);

	public boolean login(String username, String password);

	public void updateUser(Thanhvien u);

	public Thanhvien getUser(String username);

	public List<Thanhvien> getList();
}
