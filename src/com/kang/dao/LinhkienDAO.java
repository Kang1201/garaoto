package com.kang.dao;

import java.util.List;

import com.kang.model.LinhKienDichVu;

public interface LinhkienDAO {
	public void addLinhkien(LinhKienDichVu lk);

	public List<LinhKienDichVu> getList();

	public LinhKienDichVu getLinhkien(int id);

	public List<LinhKienDichVu> searchList(String ten);

	public List<LinhKienDichVu> findAllByType(int type);

}
