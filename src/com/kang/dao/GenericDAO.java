package com.kang.dao;

import java.util.List;

public interface GenericDAO<T> {
	T insert(T t);
	T update(T t);
	List<T> findAll();
	T findById(int id);
	void deleteById(int id);
	
}
