package com.kang.implement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.kang.dao.OtoDAO;
import com.kang.model.Khachhang;
import com.kang.model.Oto;

public class OtoDAOImpl implements OtoDAO{

	@Override
	public void addOto(Oto oto) {
		// TODO Auto-generated method stub
		Connection con;
		String sql = "insert into oto(id, bienso,dongxe,hanngxe,mota,khachhangId) values(?,?,?,?,?,?)";
		try {
			con = DBConnect.getConnecttion();
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(sql);
			ps.setInt(1, oto.getId());
			ps.setString(2, oto.getBienso());
			ps.setString(3, oto.getDongxe());
			ps.setString(4, oto.getHangxe());
			ps.setString(5, oto.getMota());
			ps.setInt(6, oto.getKhachhangId());
			ps.executeUpdate();
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public List<Oto> getList() {
		List<Oto> list = new ArrayList<Oto>();
		try {
			Connection con = DBConnect.getConnecttion();
			String sql = "select * from oto";
			if (con != null) {

				PreparedStatement ps = (PreparedStatement) con.prepareStatement(sql);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
//					Oto oto = (Oto) rs.getObject("oto");
					int id = rs.getInt("id");
					String bienso = rs.getString("bienso");
					String dongxe = rs.getString("dongxe");
					String hangxe = rs.getString("hanngxe");
					String mota = rs.getString("mota");
					int khachhangId = rs.getInt("khachhangId");
					list.add(new Oto(id, bienso, dongxe, hangxe, mota, khachhangId));
				}
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	public List<Oto> getListByKhachHangId(int khachHangId) {
		List<Oto> list = new ArrayList<Oto>();
		try {
			Connection con = DBConnect.getConnecttion();
			String sql = "select * from oto where khachhangId ="+khachHangId;
			if (con != null) {
				
				PreparedStatement ps = (PreparedStatement) con.prepareStatement(sql);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
//					Oto oto = (Oto) rs.getObject("oto");
					int id = rs.getInt("id");
					String bienso = rs.getString("bienso");
					String dongxe = rs.getString("dongxe");
					String hangxe = rs.getString("hanngxe");
					String mota = rs.getString("mota");
					int khachhangId = rs.getInt("khachhangId");
					list.add(new Oto(id, bienso, dongxe, hangxe, mota, khachhangId));
				}
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public Oto getOto(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}
