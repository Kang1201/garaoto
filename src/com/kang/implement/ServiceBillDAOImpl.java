package com.kang.implement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;

import com.kang.dao.GenericDAO;
import com.kang.model.Oto;
import com.kang.model.ServiceBill;

public class ServiceBillDAOImpl implements GenericDAO<ServiceBill> {

	public static void main(String[] args) {
		ServiceBillDAOImpl billDAOImpl = new ServiceBillDAOImpl();
		ServiceBill bill = new ServiceBill(0, false, new Date(), new Oto(1));
		billDAOImpl.insert(bill);
		
		KhachhangDAOImpl daoImpl =new KhachhangDAOImpl();
		daoImpl.getKhachhhang(13);

	}

	@Override
	public ServiceBill insert(ServiceBill t) {
		// TODO Auto-generated method stub
		Connection con;
		String sql = "INSERT INTO `garaoto`.`serviceBill`(`Id`, `Tinhtrang`, `tongtien`, `SlotId`, `ThanhvienId`, `checkIn`, `checkOut`, `otoId`) VALUES (?, ?, ?, ?, ?, ?, ?, ?) ";
		try {
			con = DBConnect.getConnecttion();
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setObject(1, t.getId());
			ps.setObject(2, t.isTinhtrang());
			ps.setObject(3, t.getTongtien());
			ps.setObject(4, t.getSlot() != null ? t.getSlot().getId() : null);
			ps.setObject(5, t.getThanhvien() != null ? t.getThanhvien().getId() : null);
			ps.setObject(6, t.getCheckIn());
			ps.setObject(7, t.getCheckOut());
			ps.setObject(8, t.getOto() != null ? t.getOto().getId() : null);

//				 
			ps.executeUpdate();

			ResultSet rs = ps.getGeneratedKeys();

			if (rs.next()) {
				t.setId(rs.getInt(1));
//						System.out.println("Inserted ID -" + id); // display inserted record
			}
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return t;
	}

	@Override
	public ServiceBill update(ServiceBill t) {
		Connection con;
		String sql = "UPDATE `garaoto`.`serviceBill` SET `Tinhtrang` = ?, `tongtien` = ?, `SlotId` = ?, `ThanhvienId` = ?, `checkIn` = ?, `checkOut` = ?, `otoId` = ? WHERE `Id` = ? "   
				 ;
		try {
			con = DBConnect.getConnecttion();
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(sql);
			ps.setObject(8, t.getId());
			ps.setObject(1, t.isTinhtrang());
			ps.setObject(2, t.getTongtien());
			ps.setObject(3, t.getSlot() != null ? t.getSlot().getId() : null);
			ps.setObject(4, t.getThanhvien() != null ? t.getThanhvien().getId() : null);
			ps.setObject(5, t.getCheckIn());
			ps.setObject(6, t.getCheckOut());
			ps.setObject(7, t.getOto() != null ? t.getOto().getId() : null);
			ps.executeUpdate();
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return t;
	}

	@Override
	public List<ServiceBill> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceBill findById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteById(int id) {
		// TODO Auto-generated method stub

	}

//	@Override
//	public void addLinhkien(LinhKienDichVu lk) {
//		Connection con;
//		String sql = "insert into linhkien_dichVu value(?,?,?,?,?)";
//		try {
//			con = DBConnect.getConnecttion();
//			PreparedStatement ps = (PreparedStatement) con.prepareStatement(sql);
//			ps.setInt(1, lk.getId());
//			ps.setString(2, lk.getTen());
//			ps.setString(3, lk.getHang());
//			ps.setFloat(4, lk.getDongia());
//			ps.setInt(5, lk.getSoluong());
//
//			ps.executeUpdate();
//			con.close();
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	}
//
//	@Override
//	public List<LinhKienDichVu> getList() {
//		List<LinhKienDichVu> list = new ArrayList<LinhKienDichVu>();
//		try {
//			Connection con = DBConnect.getConnecttion();
//			String sql = "select * from linhkien_dichVu";
//			if (con != null) {
//
//				PreparedStatement ps = (PreparedStatement) con.prepareStatement(sql);
//				ResultSet rs = ps.executeQuery();
//				while (rs.next()) {
//
//					int id = rs.getInt("id");
//					String ten = rs.getString("ten");
//					String hang = rs.getString("hang");
//					float dongia = rs.getFloat("dongia");
//					int soluong = rs.getInt("soluong");
//					list.add(new LinhKienDichVu(id, ten, hang, dongia, soluong));
//				}
//				con.close();
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return list;
//	}
//
//	@Override
//	public LinhKienDichVu getLinhkien(int id) {
//		LinhKienDichVu lk = new LinhKienDichVu();
//		try {
//			Connection con = DBConnect.getConnecttion();
//
//			String sql = "select * from linhkien_dichVu where id='" + id + "'";
//			PreparedStatement ps = (PreparedStatement) con.prepareStatement(sql);
//			ResultSet rs = ps.executeQuery();
//			while (rs.next()) {
//
//				id = rs.getInt("id");
//				String ten = rs.getString("ten");
//				String hang = rs.getString("hang");
//				float dongia = rs.getFloat("dongia");
//				int soluong = rs.getInt("soluong");
//				lk = new LinhKienDichVu(id, ten, hang, dongia, soluong);
//			}
//			con.close();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return lk;
//	}
//
//	@Override
//	public List<LinhKienDichVu> searchList(String ten) {
//		String sql = null;
//		if (!ten.equals("")) {
//			sql = "select * from linhkien_dichVu where ten='" + ten + "'";
//
//		}
//		List<LinhKienDichVu> list = new ArrayList<LinhKienDichVu>();
//		try {
//			Connection con = DBConnect.getConnecttion();
//			PreparedStatement ps = (PreparedStatement) con.prepareStatement(sql);
//			ResultSet rs = ps.executeQuery();
//			while (rs.next()) {
//				int id = rs.getInt("id");
//				ten = rs.getString("ten");
//				String hang = rs.getString("hang");
//				float dongia = rs.getFloat("dongia");
//				int soluong = rs.getInt("soluong");
//				list.add(new LinhKienDichVu(id, ten, hang, dongia, soluong));
//			}
//			con.close();
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return list;
//	}
//
//	@Override
//	public List<LinhKienDichVu> findAllByType(int type) {
//		List<LinhKienDichVu> list = new ArrayList<LinhKienDichVu>();
//		Connection con = null;
//		try {
//			con = DBConnect.getConnecttion();
//			String sql = "select * from linhkien_dichVu ld where ld.type =? ";
//			if (con != null) {
//
//				PreparedStatement ps = (PreparedStatement) con.prepareStatement(sql);
//				ps.setObject(1, type);
//				ResultSet rs = ps.executeQuery();
//				while (rs.next()) {
//
//					int id = rs.getInt("id");
//					String ten = rs.getString("ten");
//					String hang = rs.getString("hang");
//					float dongia = rs.getFloat("dongia");
//					int soluong = rs.getInt("soluong");
//					list.add(new LinhKienDichVu(id, ten, hang, dongia, soluong));
//				}
//				rs.close();
//				ps.close();
//
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		} finally {
//			try {
//				if (con != null) {
//					con.close();
//				}
//
//			} catch (SQLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//
//		}
//
//		return list;
//	}

}
