package com.kang.model;

public class Oto {
	private int id;

	private String bienso;
	private String dongxe;
	private String hangxe;
	private String mota;
	private int khachhangId;

	public Oto() {

	}

	public Oto(int id) {
		super();
		this.id = id;
	}

	public Oto(int id, String bienso, String dongxe, String hangxe, String mota, int khachhangId) {
		super();
		this.id = id;
		this.bienso = bienso;
		this.dongxe = dongxe;
		this.hangxe = hangxe;
		this.mota = mota;
		this.khachhangId = khachhangId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBienso() {
		return bienso;
	}

	public void setBienso(String bienso) {
		this.bienso = bienso;
	}

	public String getDongxe() {
		return dongxe;
	}

	public void setDongxe(String dongxe) {
		this.dongxe = dongxe;
	}

	public String getHangxe() {
		return hangxe;
	}

	public void setHangxe(String hangxe) {
		this.hangxe = hangxe;
	}

	public String getMota() {
		return mota;
	}

	public void setMota(String mota) {
		this.mota = mota;
	}

	public int getKhachhangId() {
		return khachhangId;
	}

	public void setKhachhangId(int khachhangId) {
		this.khachhangId = khachhangId;
	}
}
