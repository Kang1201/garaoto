package com.kang.model;

import java.sql.Date;

public class Thanhvien {
	private int id;
	private String username;
	private String password;
	private String hoten;
	private Date ngaysinh;
	private String email;
	private String sodt;
	private String ghichu;
	private boolean tinhtrang;
	
	public boolean isTinhtrang() {
		return tinhtrang;
	}

	public void setTinhtrang(boolean tinhtrang) {
		this.tinhtrang = tinhtrang;
	}

	public String getGhichu() {
		return ghichu;
	}

	public void setGhichu(String ghichu) {
		this.ghichu = ghichu;
	}

	private String diachi;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHoten() {
		return hoten;
	}

	public void setHoten(String hoten) {
		this.hoten = hoten;
	}

	public Date getNgaysinh() {
		return ngaysinh;
	}

	public void setNgaysinh(Date ngaysinh) {
		this.ngaysinh = ngaysinh;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSodt() {
		return sodt;
	}

	public void setSodt(String sdt) {
		this.sodt = sdt;
	}

	public String getDiachi() {
		return diachi;
	}

	public void setDiachi(String diachi) {
		this.diachi = diachi;
	}

	public Thanhvien(int id, String username, String password, String hoten, Date ngaysinh, String email, String sodt,
			String ghichu, String diachi, boolean tinhtrang) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.hoten = hoten;
		this.ngaysinh = ngaysinh;
		this.email = email;
		this.sodt = sodt;
		this.ghichu = ghichu;
		this.diachi = diachi;
		this.tinhtrang = tinhtrang;
	}

	public Thanhvien() {
		super();
	}

}
