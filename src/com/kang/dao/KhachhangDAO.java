package com.kang.dao;

import java.util.List;

import com.kang.model.Khachhang;


public interface KhachhangDAO {
	public void addKhachhang(Khachhang p);

	// hien thi sanh sach khach hang
	public List<Khachhang> getList();

	public Khachhang getKhachhhang(int id);

	public List<Khachhang> searchList(String hoten);

	public Object getKhachhhangByName(String name);
}
