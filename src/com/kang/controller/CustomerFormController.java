package com.kang.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kang.dao.KhachhangDAO;
import com.kang.dao.OtoDAO;
import com.kang.implement.KhachhangDAOImpl;
import com.kang.implement.OtoDAOImpl;
import com.kang.implement.ThanhvienDAOImpl;
//import com.kang.implement.UserDAOImpl;
import com.kang.model.*;
//import com.kang.sha1.MaHoa;

/**
 * Servlet implementation class DangnhapServlet
 */
@WebServlet("/customerForm")
public class CustomerFormController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ThanhvienDAOImpl thanhvienDAO = new ThanhvienDAOImpl();
	OtoDAO otoDAO = new OtoDAOImpl();

	KhachhangDAO customerDao = new KhachhangDAOImpl();
//	private List<Cart> cart = new ArrayList<Cart>();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CustomerFormController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		
 
			
		String url = "/html/customerForm.jsp";
		
		
		
		
		RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
		rd.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		// TODO Auto-generated method stub
		String description = request.getParameter("description");
		String address = request.getParameter("address");
		String email = request.getParameter("email");
		String fullName = request.getParameter("fullName");
		String phoneNumber = request.getParameter("phoneNumber");

		String err = "";
		if ("".equals(fullName) || "".equals(fullName)) {
			err += "Phải nhập đầy đủ thông tin!";
		} else {
			 
		}

		if (err.length() > 0) {
			request.setAttribute("err", err);
		}

		String url = "/Login.jsp";
		try {
			if (err.length() == 0) {
				Khachhang khachhang =new Khachhang(0, fullName, phoneNumber, description, email, address);
				customerDao.addKhachhang(khachhang);
				
				
				response.sendRedirect("customer");
				 
			} else {
				url = "customerForm";
				RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
				rd.forward(request, response);
			}

		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("customerForm");
		}
	}

}
