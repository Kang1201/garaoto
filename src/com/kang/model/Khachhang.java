package com.kang.model;

public class Khachhang {
	private Oto oto;
	private int id;
	private String hoten;
	private String sdt;
	private String ghichu;
	private String email;
	private String diachi;

	public Khachhang() {
		// TODO Auto-generated constructor stub
	}

	public Khachhang(int id, String hoten, String sdt, String ghichu, String email, String diachi) {
		super();
		this.id = id;
		this.hoten = hoten;
		this.sdt = sdt;
		this.ghichu = ghichu;
		this.email = email;
		this.diachi = diachi;
	}

	public Khachhang(Oto oto, int id, String hoten, String sdt, String ghichu, String email, String diachi) {
		super();
		this.oto = oto;
		this.id = id;
		this.hoten = hoten;
		this.sdt = sdt;
		this.ghichu = ghichu;
		this.email = email;
		this.diachi = diachi;
	}

	public String getDiachi() {
		return diachi;
	}

	public void setDiachi(String diachi) {
		this.diachi = diachi;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Oto getOto() {
		return oto;
	}

	public void setOto(Oto oto) {
		this.oto = oto;
	}

	public String getHoten() {
		return hoten;
	}

	public void setHoten(String hoten) {
		this.hoten = hoten;
	}

	public String getSdt() {
		return sdt;
	}

	public void setSdt(String sdt) {
		this.sdt = sdt;
	}

	public String getGhichu() {
		return ghichu;
	}

	public void setGhichu(String ghichu) {
		this.ghichu = ghichu;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
