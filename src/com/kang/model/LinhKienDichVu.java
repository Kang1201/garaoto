package com.kang.model;

public class LinhKienDichVu {
	private int id;
	private String ten;
	private String hang;
	private float dongia;
	private int soluong;
	
	private int type;
	

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public LinhKienDichVu() {
		// TODO Auto-generated constructor stub
	}

	public LinhKienDichVu(int id, String ten, String hang, float dongia, int soluong) {
		super();
		this.id = id;
		this.ten = ten;
		this.hang = hang;
		this.dongia = dongia;
		this.soluong = soluong;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTen() {
		return ten;
	}

	public void setTen(String ten) {
		this.ten = ten;
	}

	public String getHang() {
		return hang;
	}

	public void setHang(String hang) {
		this.hang = hang;
	}

	public float getDongia() {
		return dongia;
	}

	public void setDongia(float dongia) {
		this.dongia = dongia;
	}

	public int getSoluong() {
		return soluong;
	}

	public void setSoluong(int soluong) {
		this.soluong = soluong;
	}

}
