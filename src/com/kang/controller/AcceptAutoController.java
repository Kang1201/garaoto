package com.kang.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kang.implement.OtoDAOImpl;
import com.kang.implement.ThanhvienDAOImpl;
//import com.kang.implement.UserDAOImpl;
import com.kang.model.*;
//import com.kang.sha1.MaHoa;

/**
 * Servlet implementation class DangnhapServlet
 */
@WebServlet("/acceptAuto")
public class AcceptAutoController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ThanhvienDAOImpl thanhvienDAO = new ThanhvienDAOImpl();
	OtoDAOImpl otoDAOImpl =new OtoDAOImpl();
//	private List<Cart> cart = new ArrayList<Cart>();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AcceptAutoController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String url = "/html/auto.jsp";
		String id = request.getParameter("id");
		if(id!=null && !"".equals(id)) {
			
			int khachHangId = Integer.parseInt(id);
			List<Oto> otos =otoDAOImpl.getListByKhachHangId(khachHangId);
			
			request.setAttribute("autos", otos);
			 
		}
		request.setAttribute("khachHangId", id);
		
		RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
		rd.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String bienso = request.getParameter("bienso");
		String dongxe = request.getParameter("dongxe");
		String hangxe = request.getParameter("hangxe");
		String mota = request.getParameter("mota");
		String khachHangId = request.getParameter("khachHangId");

		String err = "";
		if ("".equals(bienso) || "".equals(bienso)) {
			err += "Phải nhập đầy đủ thông tin!";
		} else {
			 
		}

		if (err.length() > 0) {
			request.setAttribute("err", err);
		}

		String url = "/Login.jsp";
		try {
			if (err.length() == 0) {
				 Oto oto =new Oto(0, bienso, dongxe, hangxe, mota, Integer.parseInt( khachHangId));
				 otoDAOImpl.addOto(oto);
				response.sendRedirect("acceptAuto?id="+khachHangId);
			 
			} else {
				  url = "/html/auto.jsp";
				RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
				rd.forward(request, response);
			}

		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("/Login.jsp");
		}
	}

}
