package com.kang.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kang.dao.KhachhangDAO;
import com.kang.dao.OtoDAO;
import com.kang.implement.KhachhangDAOImpl;
import com.kang.implement.OtoDAOImpl;
import com.kang.implement.ThanhvienDAOImpl;
//import com.kang.implement.UserDAOImpl;
import com.kang.model.*;
//import com.kang.sha1.MaHoa;

/**
 * Servlet implementation class DangnhapServlet
 */
@WebServlet("/customer")
public class CustomerController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ThanhvienDAOImpl thanhvienDAO = new ThanhvienDAOImpl();
	OtoDAO otoDAO = new OtoDAOImpl();

	KhachhangDAO customerDao = new KhachhangDAOImpl();
//	private List<Cart> cart = new ArrayList<Cart>();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CustomerController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String name = request.getParameter("customerName");
		
		if(name!=null && !name.isEmpty()) {
			request.setAttribute("customers", customerDao.searchList(name));
		}else {
			request.setAttribute("customers", customerDao.getList());
		}
			
		String url = "/html/customers.jsp";
		
		
		
		
		RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
		rd.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String username = request.getParameter("username");
		String password = request.getParameter("password");

		String err = "";
		if ("".equals(username) || "".equals(password)) {
			err += "Phải nhập đầy đủ thông tin!";
		} else {
			if (thanhvienDAO.login(username, password)) {
				err += "Tên đăng nhập hoặc mật khẩu không chính xác!";
			}
		}

		if (err.length() > 0) {
			request.setAttribute("err", err);
		}

		String url = "/Login.jsp";
		try {
			if (err.length() == 0) {
				HttpSession session = request.getSession();
				session.setAttribute("username", username);
//				session.setAttribute("cart", cart);
				thanhvienDAO.login(username, password);
				Cookie loginCookie = new Cookie("username", username);
				// setting cookie to expiry in 30 mins
				loginCookie.setMaxAge(30 * 60);
				response.addCookie(loginCookie);
				response.sendRedirect("/html/index.html");
				url = "/html/index.html";
			} else {
				url = "/Login.jsp";
				RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
				rd.forward(request, response);
			}

		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("/Login.jsp");
		}
	}

}
