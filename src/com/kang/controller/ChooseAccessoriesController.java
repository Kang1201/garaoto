package com.kang.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kang.dao.LinhkienDAO;
import com.kang.implement.LinhkienDAOImpl;
import com.kang.implement.OtoDAOImpl;
import com.kang.implement.ServiceBillDAOImpl;
//import com.kang.implement.UserDAOImpl;
//import com.kang.sha1.MaHoa;
import com.kang.model.LinhKienDichVu;
import com.kang.model.Oto;
import com.kang.model.ServiceBill;

/**
 * Servlet implementation class DangnhapServlet
 */
@WebServlet("/chooseAccessories")
public class ChooseAccessoriesController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	OtoDAOImpl otoDAOImpl = new OtoDAOImpl();
	ServiceBillDAOImpl serviceBillDAOImpl = new ServiceBillDAOImpl();

	private LinhkienDAO linhkienDAO = new LinhkienDAOImpl();
//	private List<Cart> cart = new ArrayList<Cart>();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ChooseAccessoriesController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		String url = "/html/auto.jsp";
		String id = request.getParameter("id");
		String typeString = request.getParameter("type");
//		int type = (typeString != null && !typeString.isEmpty()) ? Integer.parseInt(typeString) : 1;

		if (id != null && !"".equals(id)) {

			ServiceBill bill = new ServiceBill(0, false, new Date(), new Oto(Integer.parseInt(id)));
			bill = serviceBillDAOImpl.insert(bill);

			request.getSession().setAttribute("bill", bill);

			List<LinhKienDichVu> linhKiens = this.linhkienDAO.findAllByType(1);
			List<LinhKienDichVu> dichVus = this.linhkienDAO.findAllByType(2);
//			int khachHangId = Integer.parseInt(id);
//			List<Oto> otos = otoDAOImpl.getListByKhachHangId(khachHangId);

			request.setAttribute("linhKiens", linhKiens);
			request.setAttribute("dichVus", dichVus);

		}
		request.setAttribute("khachHangId", id);

		RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
		rd.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String bienso = request.getParameter("bienso");
		String dongxe = request.getParameter("dongxe");
		String hangxe = request.getParameter("hangxe");
		String mota = request.getParameter("mota");
		String khachHangId = request.getParameter("khachHangId");

		String err = "";
		if ("".equals(bienso) || "".equals(bienso)) {
			err += "Phải nhập đầy đủ thông tin!";
		} else {

		}

		if (err.length() > 0) {
			request.setAttribute("err", err);
		}

		String url = "/Login.jsp";
		try {
			if (err.length() == 0) {
				Oto oto = new Oto(0, bienso, dongxe, hangxe, mota, Integer.parseInt(khachHangId));
				otoDAOImpl.addOto(oto);
				response.sendRedirect("acceptAuto?id=" + khachHangId);

			} else {
				url = "/html/auto.jsp";
				RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
				rd.forward(request, response);
			}

		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("/Login.jsp");
		}
	}

}
