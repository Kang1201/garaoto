package com.kang.implement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.kang.dao.LinhkienDAO;
import com.kang.model.LinhKienDichVu;

public class LinhkienDAOImpl implements LinhkienDAO {

	@Override
	public void addLinhkien(LinhKienDichVu lk) {
		Connection con;
		String sql = "insert into linhkien_dichVu value(?,?,?,?,?)";
		try {
			con = DBConnect.getConnecttion();
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(sql);
			ps.setInt(1, lk.getId());
			ps.setString(2, lk.getTen());
			ps.setString(3, lk.getHang());
			ps.setFloat(4, lk.getDongia());
			ps.setInt(5, lk.getSoluong());

			ps.executeUpdate();
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public List<LinhKienDichVu> getList() {
		List<LinhKienDichVu> list = new ArrayList<LinhKienDichVu>();
		try {
			Connection con = DBConnect.getConnecttion();
			String sql = "select * from linhkien_dichVu";
			if (con != null) {

				PreparedStatement ps = (PreparedStatement) con.prepareStatement(sql);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {

					int id = rs.getInt("id");
					String ten = rs.getString("ten");
					String hang = rs.getString("hang");
					float dongia = rs.getFloat("dongia");
					int soluong = rs.getInt("soluong");
					list.add(new LinhKienDichVu(id, ten, hang, dongia, soluong));
				}
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public LinhKienDichVu getLinhkien(int id) {
		LinhKienDichVu lk = new LinhKienDichVu();
		try {
			Connection con = DBConnect.getConnecttion();

			String sql = "select * from linhkien_dichVu where id='" + id + "'";
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {

				id = rs.getInt("id");
				String ten = rs.getString("ten");
				String hang = rs.getString("hang");
				float dongia = rs.getFloat("dongia");
				int soluong = rs.getInt("soluong");
				lk = new LinhKienDichVu(id, ten, hang, dongia, soluong);
			}
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return lk;
	}

	@Override
	public List<LinhKienDichVu> searchList(String ten) {
		String sql = null;
		if (!ten.equals("")) {
			sql = "select * from linhkien_dichVu where ten='" + ten + "'";

		}
		List<LinhKienDichVu> list = new ArrayList<LinhKienDichVu>();
		try {
			Connection con = DBConnect.getConnecttion();
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				ten = rs.getString("ten");
				String hang = rs.getString("hang");
				float dongia = rs.getFloat("dongia");
				int soluong = rs.getInt("soluong");
				list.add(new LinhKienDichVu(id, ten, hang, dongia, soluong));
			}
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<LinhKienDichVu> findAllByType(int type) {
		List<LinhKienDichVu> list = new ArrayList<LinhKienDichVu>();
		Connection con = null;
		try {
			con = DBConnect.getConnecttion();
			String sql = "select * from linhkien_dichVu ld where ld.type =? ";
			if (con != null) {

				PreparedStatement ps = (PreparedStatement) con.prepareStatement(sql);
				ps.setObject(1, type);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {

					int id = rs.getInt("id");
					String ten = rs.getString("ten");
					String hang = rs.getString("hang");
					float dongia = rs.getFloat("dongia");
					int soluong = rs.getInt("soluong");
					list.add(new LinhKienDichVu(id, ten, hang, dongia, soluong));
				}
				rs.close();
				ps.close();

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null) {
					con.close();
				}

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		return list;
	}

}
