﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<title>GaraOto</title>
<link rel="icon" href="favicon.ico" type="image/x-icon">
<link href="assets/plugins/bootstrap/css/bootstrap.min.css"
	rel="stylesheet" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" type="text/css">
<!-- Dropzone Css -->
<link href="assets/plugins/dropzone/dropzone.css" rel="stylesheet">
<!-- Bootstrap Material Datetime Picker Css -->
<link
	href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"
	rel="stylesheet" />
<!-- Wait Me Css -->
<link href="assets/plugins/waitme/waitMe.css" rel="stylesheet" />
<!-- Bootstrap Select Css -->
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css"
	rel="stylesheet" />

<link href="assets/css/main.css" rel="stylesheet">
<!-- Custom Css -->

<!-- Swift Themes. You can choose a theme from css/themes instead of get all themes -->
<link href="assets/css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-cyan">
	
	<section class="content">
		<div class="container-fluid">
			<div class="block-header">
				<h2>Add Customer</h2>
				<small class="text-muted">Welcome to application</small>
			</div>
			<div class="row clearfix">
				<div class="col-lg-12 col-md-12 col-sm-12">
					<div class="card">
						<div class="header">
							<h2>
								Basic Information <small>Description text here...</small>
							</h2>
							<ul class="header-dropdown m-r--5">
								<li class="dropdown"><a href="javascript:void(0);"
									class="dropdown-toggle" data-toggle="dropdown" role="button"
									aria-haspopup="true" aria-expanded="false"><i
										class="zmdi zmdi-more-vert"></i></a>
									<ul class="dropdown-menu pull-right">
										<li><a href="javascript:void(0);"
											class=" waves-effect waves-block">Action</a></li>
										<li><a href="javascript:void(0);"
											class=" waves-effect waves-block">Another action</a></li>
										<li><a href="javascript:void(0);"
											class=" waves-effect waves-block">Something else here</a></li>
									</ul></li>
							</ul>
						</div>

						<form action="customerForm" method="post">
							<div class="body">
								<div class="row clearfix">
									<div class="col-lg-3 col-sm-12">
										<div class="form-group">
											<div class="form-line">
												<input name="fullName" type="text" class="form-control"
													placeholder="Họ và Tên">
											</div>
										</div>
									</div>
									<div class="col-lg-3 col-sm-12">
										<div class="form-group">
											<div class="form-line">
												<input name="phoneNumber" type="text" class="form-control"
													placeholder="SĐT">
											</div>
										</div>
									</div>
									<div class="col-lg-3 col-sm-12">
										<div class="form-group">
											<div class="form-line">
												<input name="email" type="text" class="form-control"
													placeholder="Email">
											</div>
										</div>
									</div>
									<div class="col-lg-3 col-sm-12">
										<div class="form-group">
											<div class="form-line">
												<input name="address" type="text" class="form-control"
													placeholder="Địa chỉ">
											</div>
										</div>
									</div>
									<%-- <div class="col-lg-3 col-md-6 col-sm-12">
										<div class="form-group drop-custum">
											<select name="customerId" class="form-control show-tick">
											<option value="">-- Khách hàng --</option>
											<c:forEach items="${customers}" var="customer">
											<option value="${customer.id }">${customer.hoten }</option>
											
											</c:forEach>
												
												
											</select>
										</div>
									</div> --%>

								</div>

								<div class="row clearfix">
									<div class="col-sm-12">
										<div class="form-group">
											<div class="form-line">
												<textarea name="description" rows="4" class="form-control no-resize"
													placeholder="Description"></textarea>
											</div>
										</div>
									</div>
									<div class="col-xs-12">
										<button type="submit" class="btn btn-raised g-bg-cyan">Submit</button>
										<button type="submit" class="btn btn-raised">Cancel</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>

		</div>
	</section>

	<div class="color-bg"></div>
	<!-- Jquery Core Js -->
	<script src="assets/bundles/libscripts.bundle.js"></script>
	<!-- Lib Scripts Plugin Js -->
	<script src="assets/bundles/morphingsearchscripts.bundle.js"></script>
	<!-- morphing search Js -->
	<script src="assets/bundles/vendorscripts.bundle.js"></script>
	<!-- Lib Scripts Plugin Js -->


	<script src="assets/plugins/autosize/autosize.js"></script>
	<!-- Autosize Plugin Js -->
	<script src="assets/plugins/momentjs/moment.js"></script>
	<!-- Moment Plugin Js -->
	<script src="assets/plugins/dropzone/dropzone.js"></script>
	<!-- Dropzone Plugin Js -->
	<!-- Bootstrap Material Datetime Picker Plugin Js -->
	<script
		src="assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

	<script src="assets/bundles/mainscripts.bundle.js"></script>
	<!-- Custom Js -->
	<script src="assets/bundles/morphingscripts.bundle.js"></script>
	<!-- morphing search page js -->
	<script src="assets/js/pages/forms/basic-form-elements.js"></script>
</body>
</html>