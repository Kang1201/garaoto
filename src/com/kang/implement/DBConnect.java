package com.kang.implement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnect {

	public static Connection getConnecttion() throws SQLException {
		Connection cons = null;

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			cons = DriverManager.getConnection("jdbc:mysql://localhost:3306/garaoto?useUnicode=true&characterEncoding=utf-8", "root", "popeye98");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return cons;

	}

	public static void main(String[] args) throws SQLException {
		System.out.println(getConnecttion());
	}
}
