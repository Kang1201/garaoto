package com.kang.test;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.Assert;
import org.junit.Test;

import com.kang.implement.DBConnect;
import com.kang.implement.KhachhangDAOImpl;
import com.kang.model.Khachhang;

 

public class garaotoTest {
	@Test  
	public void testAddKhachhang() throws SQLException{
	    KhachhangDAOImpl cd = new KhachhangDAOImpl();
	    Khachhang khachhang = new Khachhang(5, "nguyen hoang", "0376890765", null, "hoang@gmail.com", "Ha Noi");
//	    Connection con = cd.con;
	    try{
//	        con.setAutoCommit(false);
	        cd.addKhachhang(khachhang);
	        Assert.assertNotNull(khachhang);
	        Assert.assertTrue(3 < khachhang.getId()); // test correct id
	        Assert.assertEquals(8, cd.getList().size()); // test all row in the table
	 
	        //test the new inserted row
	        Khachhang client = cd.getKhachhhang(khachhang.getId());
	        Assert.assertEquals(khachhang.getHoten(), client.getHoten());
	        Assert.assertEquals(khachhang.getEmail(), client.getEmail());
	        Assert.assertEquals(khachhang.getSdt() , client.getSdt());
	        Assert.assertEquals(khachhang.getDiachi() , client.getDiachi());
	        Assert.assertEquals(khachhang.getGhichu(), client.getGhichu());
	    }catch(Exception e){
	        e.printStackTrace();
	    }finally{
	        try{
//	            con.rollback();
//	            con.setAutoCommit(true);
	        }catch(Exception e){
	            e.printStackTrace();
	        }
	    }
	    return;
	}

//// kich ban chuan get client by id
//	@Test
//	public void testGetClientById() {
//		KhachhangDAOImpl khdao = new KhachhangDAOImpl();
//		Khachhang kh = khdao.getKhachhhang(1);
//		Assert.assertNotNull(kh);
//	}
//
//	@Test
//	public void testGetClientById1() {
//		KhachhangDAOImpl khdao = new KhachhangDAOImpl();
//		Khachhang kh = khdao.getKhachhhang(2);
//		Assert.assertNotNull(kh);
//	}
//
//	@Test
//	public void testGetClientById2() {
//		KhachhangDAOImpl khdao = new KhachhangDAOImpl();
//		Khachhang kh = khdao.getKhachhhang(3);
//		Assert.assertNotNull(kh);
//	}
//
//	@Test
//	public void testGetClientById3() {
//		KhachhangDAOImpl khdao = new KhachhangDAOImpl();
//		Khachhang kh = khdao.getKhachhhang(4);
//		Assert.assertNotNull(kh);
//	}
//
//	@Test
//	public void testGetClientById4() {
//		KhachhangDAOImpl khdao = new KhachhangDAOImpl();
//		Khachhang kh = khdao.getKhachhhang(5);
//		Assert.assertNotNull(kh);
//	}
//// test lay danh sach khach hang
//	@Test
//	public void testGetAllClient() {
//		KhachhangDAOImpl cd = new KhachhangDAOImpl();
//		ArrayList<Khachhang> listClient = (ArrayList<Khachhang>) cd.getList();
//		Assert.assertNotNull(listClient);
//		Assert.assertEquals(6, listClient.size());
//		return;
//	}
//// Test Add Oto
//	@Test  
//	public void testAddOto(){
//	    OtoDAOImpl cd = new OtoDAOImpl();
//	    Oto oto = new Oto(1, "111A1", "Ban Tai", "Honda", "Mau Den", 1) ;
//	    Connection con = cd.con;        
//	    try{
//	        con.setAutoCommit(false);
//	        cd.addOto(oto);
////	        cd.addKhachhang(khachhang);
//	        Assert.assertNotNull(oto);
//	        Assert.assertTrue(3 > oto.getId()); // test correct id
//	        Assert.assertEquals(1, cd.getList().size()); // test all row in the table
//	 
//	        //test the new inserted row
//	        
//	    }catch(Exception e){
//	        e.printStackTrace();
//	    }finally{
//	        try{
//	            con.rollback();
//	            con.setAutoCommit(true);
//	        }catch(Exception e){
//	            e.printStackTrace();
//	        }
//	    }
//	    return;
//	}
////	test lay danh sach oto
//	@Test
//	public void testGetAllOto() {
//		OtoDAOImpl cd = new OtoDAOImpl();
//		ArrayList<Oto> listOto = (ArrayList<Oto>) cd.getList();
//		Assert.assertNotNull(listOto);
//		Assert.assertEquals(0, listOto.size());
//		return;
//	}
////	Test lay danh sach linh kien
//	@Test
//	public void testGetAllLinhKien() {
//		LinhkienDAOImpl cd= new LinhkienDAOImpl();
//		ArrayList<Linhkien> listLinhkien = (ArrayList<Linhkien>) cd.getList();
//		Assert.assertNotNull(listLinhkien);
//		Assert.assertEquals(4, listLinhkien.size());
//		return;
//	}
//	@Test
//	public void testGetAllDichvu() {
//		DichvuDAOImpl cd= new DichvuDAOImpl();
//		ArrayList<Dichvu> listDv = (ArrayList<Dichvu>) cd.getList();
//		Assert.assertNotNull(listDv);
//		Assert.assertEquals(4, listDv.size());
//		return;
//	}
//	@Test
//	public void testGetLinhkienById() {
//		LinhkienDAOImpl lkdao = new LinhkienDAOImpl();
//		Linhkien lk = lkdao.getLinhkien(1);
//		Assert.assertNotNull(lk);
//	}
//	@Test
//	public void testGetLinhkienById1() {
//		LinhkienDAOImpl lkdao = new LinhkienDAOImpl();
//		Linhkien lk = lkdao.getLinhkien(2);
//		Assert.assertNotNull(lk);
//	}
//	@Test
//	public void testGetLinhkienById2() {
//		LinhkienDAOImpl lkdao = new LinhkienDAOImpl();
//		Linhkien lk = lkdao.getLinhkien(3);
//		Assert.assertNotNull(lk);
//	}
//	@Test
//	public void testGetDichvuById() {
//		DichvuDAOImpl dvdao = new DichvuDAOImpl();
//		Dichvu dv = dvdao.getDichvu(1);
//		Assert.assertNotNull(dv);
//	}
//	@Test
//	public void testGetDichvuById1() {
//		DichvuDAOImpl dvdao = new DichvuDAOImpl();
//		Dichvu dv = dvdao.getDichvu(2);
//		Assert.assertNotNull(dv);
//	}
//	@Test
//	public void testGetDichvuById2() {
//		DichvuDAOImpl dvdao = new DichvuDAOImpl();
//		Dichvu dv = dvdao.getDichvu(3);
//		Assert.assertNotNull(dv);
//	}
}

