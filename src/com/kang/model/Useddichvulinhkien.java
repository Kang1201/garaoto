package com.kang.model;

public class Useddichvulinhkien {
	private LinhKienDichVu linhkien;
	private int id;
	private int soluong;
	private float dongia;
	private float tonggia;
	private ServiceBill serviceBill;
	

	public ServiceBill getServiceBill() {
		return serviceBill;
	}

	public void setServiceBill(ServiceBill serviceBill) {
		this.serviceBill = serviceBill;
	}

	public Useddichvulinhkien() {
		// TODO Auto-generated constructor stub
	}

	public LinhKienDichVu getLinhkien() {
		return linhkien;
	}

	public void setLinhkien(LinhKienDichVu linhkien) {
		this.linhkien = linhkien;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSoluong() {
		return soluong;
	}

	public void setSoluong(int soluong) {
		this.soluong = soluong;
	}

	public float getDongia() {
		return dongia;
	}

	public void setDongia(float dongia) {
		this.dongia = dongia;
	}

	public float getTonggia() {
		return tonggia;
	}

	public void setTonggia(float tonggia) {
		this.tonggia = tonggia;
	}

	 
}
