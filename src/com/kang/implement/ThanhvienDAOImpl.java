package com.kang.implement;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.kang.dao.ThanhvienDAO;
import com.kang.model.Thanhvien;


public class ThanhvienDAOImpl implements ThanhvienDAO {

	@Override
	public boolean checkUser(String username) {
		try {
			Connection con = DBConnect.getConnecttion();
			String sql = "select * from thanhvien where username='" + username + "'";
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				con.close();
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean login(String username, String password) {
		try {
			Connection con = DBConnect.getConnecttion();
			String sql = "select * from thanhvien where username='" + username + "' and password='" + password + "'";

			PreparedStatement ps = (PreparedStatement) con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				con.close();
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void updateUser(Thanhvien u) {
		// TODO Auto-generated method stub

	}

	@Override
	public Thanhvien getUser(String username) {
		Thanhvien t = new Thanhvien();
		try {
			Connection con = DBConnect.getConnecttion();
			String sql = "select * from thanhvien where username='" + username + "'";
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				username = rs.getString("username");
				String password = rs.getString("password");
				String hoten = rs.getString("hoten");
				Date ngaysinh = rs.getDate("ngaysinh");
				String email = rs.getString("email");
				String sodt = rs.getString("sodt");
				String ghichu = rs.getString("ghichu");
				String diachi = rs.getString("diachi");
				Boolean tinhtrang = rs.getBoolean("tinhtrang");
				t = new Thanhvien(id, username, password, hoten, ngaysinh, email, sodt, ghichu, diachi, tinhtrang);
				con.close();
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return t;
	}

	@Override
	public List<Thanhvien> getList() {
		List<Thanhvien> list = new ArrayList<Thanhvien>();
		try {
			Connection con = DBConnect.getConnecttion();
			String sql = "select * from thanhvien";
			if (con != null) {

				PreparedStatement ps = (PreparedStatement) con.prepareStatement(sql);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					int id = rs.getInt("Id");
					String username = rs.getString("Username");
					String password = rs.getString("Password");
					String hoten = rs.getString("Hoten");
					Date ngaysinh = rs.getDate("Ngaysinh");
					String email = rs.getString("Email");
					String sodt = rs.getString("Sodt");
					String ghichu = rs.getString("Ghichu");
					String diachi = rs.getString("Diachi");
					Boolean tinhtrang = rs.getBoolean("Tinhtrang");
					list.add(new Thanhvien(id, username, password, hoten, ngaysinh, email, sodt, ghichu, diachi,
							tinhtrang));
				}
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	public static void main(String[] args) {
		ThanhvienDAOImpl dao = new ThanhvienDAOImpl();
		
//		System.out.println(dao.checkUser("admin"));
		System.out.println(dao.login("Admin", "123456"));
//		System.out.println(dao.getList());
	}

}
