package com.kang.dao;

import java.util.List;

import com.kang.model.Oto;

public interface OtoDAO {
	public void addOto(Oto oto);
	public List<Oto> getList();
	public Oto getOto(int id);
}
