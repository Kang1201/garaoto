package com.kang.model;

import java.util.Date;

public class ServiceBill {
	private Slot slot;
	private Thanhvien thanhvien;
	private int id;
	private boolean tinhtrang= false;
	private float tongtien;
	private Date checkIn;
	private Date checkOut;
	
	private Oto oto;
	

	
	
	public ServiceBill(int id, boolean tinhtrang, Date checkIn, Oto oto) {
		super();
		this.id = id;
		this.tinhtrang = tinhtrang;
		this.checkIn = checkIn;
		this.oto = oto;
	}

	public Oto getOto() {
		return oto;
	}

	public void setOto(Oto oto) {
		this.oto = oto;
	}

	public Date getCheckIn() {
		return checkIn;
	}

	public void setCheckIn(Date checkIn) {
		this.checkIn = checkIn;
	}

	public Date getCheckOut() {
		return checkOut;
	}

	public void setCheckOut(Date checkOut) {
		this.checkOut = checkOut;
	}

	public ServiceBill() {
		// TODO Auto-generated constructor stub
	}

	public ServiceBill(Slot slot, Thanhvien thanhvien, int id, boolean tinhtrang, float tongtien) {
		super();
		this.slot = slot;
		this.thanhvien = thanhvien;
		this.id = id;
		this.tinhtrang = tinhtrang;
		this.tongtien = tongtien;
	}

	public Slot getSlot() {
		return slot;
	}

	public void setSlot(Slot slot) {
		this.slot = slot;
	}

	public Thanhvien getThanhvien() {
		return thanhvien;
	}

	public void setThanhvien(Thanhvien thanhvien) {
		this.thanhvien = thanhvien;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isTinhtrang() {
		return tinhtrang;
	}

	public void setTinhtrang(boolean tinhtrang) {
		this.tinhtrang = tinhtrang;
	}

	public float getTongtien() {
		return tongtien;
	}

	public void setTongtien(float tongtien) {
		this.tongtien = tongtien;
	}
}
