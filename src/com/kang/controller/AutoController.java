package com.kang.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kang.implement.OtoDAOImpl;
import com.kang.implement.ThanhvienDAOImpl;
//import com.kang.implement.UserDAOImpl;
import com.kang.model.Oto;
//import com.kang.sha1.MaHoa;

/**
 * Servlet implementation class DangnhapServlet
 */
@WebServlet("/auto")
public class AutoController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ThanhvienDAOImpl thanhvienDAO = new ThanhvienDAOImpl();
//	OtoDAO otoDAO =new OtoDAOImpl();
	OtoDAOImpl otoDAOImpl =new OtoDAOImpl();
//	private List<Cart> cart = new ArrayList<Cart>();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AutoController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String url = "/html/auto.jsp";
		request.setAttribute("customers", otoDAOImpl.getList());
		
		
		
		RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
		rd.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String bienso = request.getParameter("bienSo");
		String dongxe = request.getParameter("dongXe");
		String hangxe = request.getParameter("hangXe");
		String mota = request.getParameter("mota");
		String khachHangId = request.getParameter("khachHangId");

		String err = "";
		if ("".equals(bienso) || "".equals(bienso)) {
			err += "Phải nhập đầy đủ thông tin!";
		} else {
			 
		}

		if (err.length() > 0) {
			request.setAttribute("err", err);
		}

		String url = "/Login.jsp";
		try {
			if (err.length() == 0) {
				 Oto oto =new Oto(0, bienso, dongxe, hangxe, mota, Integer.parseInt( khachHangId));
				 otoDAOImpl.addOto(oto);
				response.sendRedirect("acceptAuto?id="+khachHangId);
			 
			} else {
				  url = "/html/auto.jsp";
				RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
				rd.forward(request, response);
			}

		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("/Login.jsp");
		}
	}

}
