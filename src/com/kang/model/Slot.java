package com.kang.model;

public class Slot {
	private int id;
	private String ten;
	private boolean tinhtrang;

	public Slot() {
		// TODO Auto-generated constructor stub
	}

	public Slot(int id, String ten, boolean tinhtrang) {
		super();
		this.id = id;
		this.ten = ten;
		this.tinhtrang = tinhtrang;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTen() {
		return ten;
	}

	public void setTen(String ten) {
		this.ten = ten;
	}

	public boolean isTinhtrang() {
		return tinhtrang;
	}

	public void setTinhtrang(boolean tinhtrang) {
		this.tinhtrang = tinhtrang;
	}

}
